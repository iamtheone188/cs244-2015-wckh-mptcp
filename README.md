# CS 244 '15 Project 3: Application Delay in MPTCP

Kevin Han and Wen-Chien Chen

## Dependencies

* Linux 3.14 with patched MPTCP (based on 2014/05/17 snapshot)
    * Patched MPTCP and pre-built kernel headers and image for Ubuntu 14.04 LTS
      available in kernel_compilation/
* Python 2.7 with NumPy, SciPy and matplotlib
    * Ubuntu packages: python-numpy, python-scipy, python-matplotlib
* Mininet 2.2.1

An Amazon EC2 AMI with all dependencies pre-installed is available in the US
West 2 region under the ID `ami-dbc9f6eb`. The sample results were generated
using a c3.xlarge instance.

## Running the experiments

To generate the application delay graph, simply run `sudo ./run.sh`. It should
take about 10 minutes. The resulting graph is `results.png`.

To run an individual measurement experiment, run `sudo python ./mptcp.py`. The
following parameters can be specified:

```
  --mptcp               Enable MPTCP
  --opti                Enable MPTCP optimizations (ignored if --mptcp is not
                        specified)
  --TCPOver3G           Run TCP over 3G instead of WiFi (ignored if --mptcp is
                        specified)
  --bwWiFi              WiFi Bandwidth in Mbps
  --bw3g                3G Bandwidth in Mbps
  --lossWiFi            WiFi Loss Rate (0-100)
  --loss3g              3G Loss Rate (0-100)
  --jitterWiFi          WiFi Jitter Rate (ms)
  --jitter3g            3G Jitter Rate (ms)
  --time TIME, -t TIME  Number of seconds to run the experiment for
```

Each experiment would generate two files, one containing the sender timestamps
for each packet, the other containing the receiver timestamps. To plot the PDF
of the latencies, run `python ./plot.py -o OUTPUT_FILENAME`. You may need to
edit the list of experiments at the bottom of `plot.py` if you wish to plot
only some of the experiments.

The sensitivity analysis script is `run_sensitivity.sh`. This varies the WiFi
bandwidth (8/16/32/64Mbps) and generates one graph for each case.
