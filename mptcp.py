# MPTCP tests

from mininet.topo import Topo
from mininet.node import CPULimitedHost
from mininet.link import TCLink
from mininet.net import Mininet
from mininet.log import lg, info
from mininet.util import dumpNodeConnections
from mininet.cli import CLI

from subprocess import Popen, PIPE
from time import sleep, time
from multiprocessing import Process
from argparse import ArgumentParser
from threading import Thread

import sys
import os
import math
import shlex

# Parse arguments
parser = ArgumentParser(description="MPTCP Application Level Latency Tests")

parser.add_argument('--mptcp', action='store_true', help="Enable or disable MPTCP")

parser.add_argument('--opti', action='store_true', help="Enable or disable MPTCP optimizations")

parser.add_argument('--TCPOver3G', action='store_true', help="Run TCP over 3G instead of WiFi")

parser.add_argument('--lossWiFi',
                    dest="lossWiFi",
                    help="WiFi Loss Rate (0-100)",
                    type=int,
                    default=0)

parser.add_argument('--loss3g',
                    dest="loss3g",
                    help="3G Loss Rate (0-100)",
                    type=int,
                    default=0)

parser.add_argument('--jitterWiFi',
                    dest="jitterWiFi",
                    help="WiFi Jitter Rate (ms)",
                    type=float,
                    default=0.0)

parser.add_argument('--jitter3g',
                    dest="jitter3g",
                    help="3G Jitter Rate (ms)",
                    type=float,
                    default=50.0)

parser.add_argument('--time', '-t',
                    dest="time",
                    help="Number of seconds to send for",
                    type=int,
                    default=120)

parser.add_argument('--bwWiFi',
                    dest="bwWiFi",
                    help="WiFi Bandwidth in Mbps",
                    type=float,
                    default=8.0)

parser.add_argument('--bw3g',
                    dest="bw3g",
                    help="3G Bandwidth in Mbps",
                    type=float,
                    default=2.0)

# Expt parameters
args = parser.parse_args()

class MPTCPTopo(Topo):
    "Topology for MPTCP experiment."

    def build(self):
        # Hosts
        h1 = self.addHost('h1') # Receiver
        h2 = self.addHost('h2') # Sender

        # Switches
        s0 = self.addSwitch('s0') # WiFi Switch
        s1 = self.addSwitch('s1') # 3G Switch

        # Loss rates and jitter are not specified, we will need to test different values
        # We will assume (like the previous group) that all packets are 1500 bytes long for queue size
        # Queue size for WiFi: 8 Mbps * 0.08s / 1500 bytes per packet = 53.33 packets
        # Queue size for 3G: 2 Mbps * 2s / 1500 bytes per packet = 333.33 packets

        # WiFi Links
        self.addLink(h2, s0, bw=args.bwWiFi, delay="10ms", loss=args.lossWiFi, jitter=str(args.jitterWiFi) + "ms", max_queue_size=54)
        self.addLink(h1, s0, bw=1000, delay="0.1ms", max_queue_size=1000)

        # 3G Links
        self.addLink(h2, s1, bw=args.bw3g, delay="75ms", loss=args.loss3g, jitter=str(args.jitter3g) + "ms", max_queue_size=334)
        self.addLink(h1, s1, bw=1000, delay="0.1ms", max_queue_size=1000)

def set_system_vars(MPTCPFlag, OptimizationsFlag):
    if MPTCPFlag:
        print "Setting mptcp_enabled flag"
        sysctl_output = Popen("sysctl -w net.mptcp.mptcp_enabled=1", stdout=PIPE, stderr=PIPE,
                              shell=True).communicate()
        print sysctl_output

        if OptimizationsFlag:
            print "Setting mptcp optimization flags"
            sysctl_output = Popen("sysctl -w net.mptcp.mptcp_rbuf_opti=1", stdout=PIPE, stderr=PIPE,
                              shell=True).communicate()
            print sysctl_output
            sysctl_output = Popen("sysctl -w net.mptcp.mptcp_rbuf_penal=1", stdout=PIPE, stderr=PIPE,
                              shell=True).communicate()
            print sysctl_output
            sysctl_output = Popen("sysctl -w net.mptcp.mptcp_rbuf_retr=1", stdout=PIPE, stderr=PIPE,
                              shell=True).communicate()
            print sysctl_output

        else:
            print "Disabling mptcp optimization flags"
            sysctl_output = Popen("sysctl -w net.mptcp.mptcp_rbuf_opti=0", stdout=PIPE, stderr=PIPE,
                              shell=True).communicate()
            print sysctl_output
            sysctl_output = Popen("sysctl -w net.mptcp.mptcp_rbuf_penal=0", stdout=PIPE, stderr=PIPE,
                              shell=True).communicate()
            print sysctl_output
            sysctl_output = Popen("sysctl -w net.mptcp.mptcp_rbuf_retr=0", stdout=PIPE, stderr=PIPE,
                              shell=True).communicate()
            print sysctl_output

    else:
        print "Disabling mptcp_enabled flag"
        sysctl_output = Popen("sysctl -w net.mptcp.mptcp_enabled=0", stdout=PIPE, stderr=PIPE,
                              shell=True).communicate()
        print sysctl_output

def set_network(net, MPTCPFlag):
    # Recall that s0 is the WiFi (1 subnet) switch and s1 is the 3G (2 subnet) switch.
    # We will set the client to 10.1.*.10 and server to 10.1.*.20 (* denotes subnet)

    h1, h2 = net.get('h1', 'h2')
    print "Setting IP addresses for client and server"
    h1.cmd('ifconfig h1-eth0 10.1.1.10/24')
    h1.cmd('ifconfig h1-eth1 10.1.2.10/24')
    h2.cmd('ifconfig h2-eth0 10.1.1.20/24')
    h2.cmd('ifconfig h2-eth1 10.1.2.20/24')

    if MPTCPFlag:
        print "Setting MPTCP IP rules and routes"
        # Set up MPTCP routing
        # Code taken from (http://multipath-tcp.org/pmwiki.php/Users/ConfigureRouting))
        # This creates two different routing tables, that we use based on the source-address.
        h1.cmd('ip rule add from 10.1.1.10 table 1')
        h1.cmd('ip rule add from 10.1.2.10 table 2')

        # Configure the two different routing tables
        h1.cmd('ip route add 10.1.1.0/24 dev h1-eth0 scope link table 1')
        h1.cmd('ip route add default via 10.1.1.1 dev h1-eth0 table 1')
        h1.cmd('ip route add 10.1.2.0/24 dev h1-eth1 scope link table 2')
        h1.cmd('ip route add default via 10.1.2.1 dev h1-eth1 table 2')

        # Now do the same for h2
        h2.cmd('ip rule add from 10.1.1.20 table 1')
        h2.cmd('ip rule add from 10.1.2.20 table 2')
        h2.cmd('ip route add 10.1.1.0/24 dev h2-eth0 scope link table 1')
        h2.cmd('ip route add default via 10.1.1.1 dev h2-eth0 table 1')
        h2.cmd('ip route add 10.1.2.0/24 dev h2-eth1 scope link table 2')
        h2.cmd('ip route add default via 10.1.2.1 dev h2-eth1 table 2')

        # For MPTCP to learn which links exist, we will need to ping the links
        print "Pinging receiver for MPTCP link learning"
        ping_output = h2.cmd("ping -c 5 10.1.1.10")
        print ping_output
        ping_output = h2.cmd("ping -c 5 10.1.2.10")
        print ping_output

def main():
    # Create the topology
    topo = MPTCPTopo()
    net = Mininet(topo=topo, host=CPULimitedHost, link=TCLink)
    net.start()
    # This dumps the topology and how nodes are interconnected through
    # links.
    dumpNodeConnections(net.hosts)
    # This performs a basic all pairs ping test.
    net.pingAll()
    # Next we will set all system variables (sysctl) and routing tables (if necessary)
    set_system_vars(args.mptcp, args.opti)
    set_network(net, args.mptcp)
    # Uncomment line below to debug network
    # CLI(net)

    # Now we run the client and server
    h1, h2 = net.get('h1', 'h2')
    print "Running client and server"
    if args.mptcp:
        if args.opti:
            server_command = "python receiver.py -p 5001 -o MPTCPOpti-receiver.txt"
            server_args = shlex.split(server_command)
            server = h1.popen(server_args)
            client_command = "python sender.py -s 10.1.1.10 -p 5001 -o MPTCPOpti-sender.txt -t %d" % args.time
            client_args = shlex.split(client_command)
            client = h2.popen(client_args)
            client.wait()
            server.wait()
        else:
            server_command = "python receiver.py -p 5001 -o MPTCPNonOpti-receiver.txt"
            server_args = shlex.split(server_command)
            server = h1.popen(server_args)
            client_command = "python sender.py -s 10.1.1.10 -p 5001 -o MPTCPNonOpti-sender.txt -t %d" % args.time
            client_args = shlex.split(client_command)
            client = h2.popen(client_args)
            client.wait()
            server.wait()

    else:
        if args.TCPOver3G:
            server_command = "python receiver.py -p 5001 -o TCP3G-receiver.txt"
            server_args = shlex.split(server_command)
            server = h1.popen(server_args)
            client_command = "python sender.py -s 10.1.2.10 -p 5001 -o TCP3G-sender.txt -t %d" % args.time
            client_args = shlex.split(client_command)
            client = h2.popen(client_args)
            client.wait()
            server.wait()
        else:
            server_command = "python receiver.py -p 5001 -o TCPWiFi-receiver.txt"
            server_args = shlex.split(server_command)
            server = h1.popen(server_args)
            client_command = "python sender.py -s 10.1.1.10 -p 5001 -o TCPWiFi-sender.txt -t %d" % args.time
            client_args = shlex.split(client_command)
            client = h2.popen(client_args)
            client.wait()
            server.wait()

    # Cleanup
    print "Disabling mptcp_enabled flag"
    sysctl_output = Popen("sysctl -w net.mptcp.mptcp_enabled=0", stdout=PIPE, stderr=PIPE,
                          shell=True).communicate()
    print sysctl_output
    os.system("killall -9 top mnexec; mn -c")

if __name__ == '__main__':
    try:
        main()
    except:
        print "-"*80
        print "Caught exception.  Cleaning up..."
        print "-"*80
        import traceback
        traceback.print_exc()
        os.system("killall -9 top mnexec; mn -c")
